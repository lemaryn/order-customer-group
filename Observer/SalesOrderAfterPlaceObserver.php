<?php
namespace Lemaryn\OrderCustomerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesOrderAfterPlaceObserver implements ObserverInterface
{
	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $scopeConfig;

	public function __construct(
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	)
	{
		$this->scopeConfig = $scopeConfig;
	}

	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		/** @var \Magento\Sales\Model\Order $order */
		$order = $observer->getOrder();

		$enable = $this->scopeConfig->getValue(
			'customergroupstatus/order/enable',
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE
		);
		if (!$enable) return;
		$customerGroup = $this->scopeConfig->getValue(
			'customergroupstatus/order/customer_group',
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE
		);
		$orderStatus = $this->scopeConfig->getValue(
			'customergroupstatus/order/order_status',
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE
		);

		if($customerGroup && $orderStatus) {
			$customer = $order->getCustomer();
			if(in_array($customer->getGroupId(), $customerGroup)) {
				$order->setState( $orderStatus )->setStatus( $orderStatus );
				$order->save();
			}
		}
	}
}
